namespace SeleniumDemo.Constants
{
    public static class NavigationDropdownTypes
    {
        public static string Home = "Początek";
        public static string Students = "Dla studentów";
        public static string Teachers = "Dla nauczycieli";
    }
}