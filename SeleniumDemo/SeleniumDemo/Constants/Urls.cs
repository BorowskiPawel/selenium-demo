namespace SeleniumDemo.Constants
{
    public static class Urls
    {
        public static string Base => "https://degra.wi.pb.edu.pl";
        public static string Home => Base + "/rozklady/rozklad.php?page=home";
        
        public static class Students
        {
            public static string Stationary => Base + "/rozklady/rozklad.php?page=st";
            public static string NonStationary => Base + "/rozklady/rozklad.php?page=nst";
            public static string AfterDiploma => Base + "/rozklady/rozklad.php?page=pd";
        
            public static string TeacherSchedule => Base + "/rozklady/rozklad.php?page=nau";
        }
        
        public static class Teachers
        {
            public static string RoomsAvailiability => Base + "/rozklady/rozklad.php?page=sal";
            public static string FindFreeRooms => Base + "/rozklady/rozklad.php?page=wolne";
            public static string Timeschedule => Base + "/rozklady/rozklad.php?page=nau";
            public static string EditConsultations => Base + "/rozklady/rozklad.php?page=konedit";
            public static string Consultations => Base + "/rozklady/rozklad.php?page=konster";
        }
    }
}