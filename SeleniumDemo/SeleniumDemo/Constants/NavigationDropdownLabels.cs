namespace SeleniumDemo.Constants
{
    public static class NavigationDropdownLabels
    {
        public static string Home => "Początek";
        public static string StationaryStudies => "Studia stacjonarne";
        public static string NonStationaryStudies => "Studia niestacjonarne";
        public static string AfterDiplomaStudies => "Studia podyplomowe";
        public static string TeacherSchedule => "Rozkład nauczyciela";
        
        public static string RoomsAvailiability => "Zajętość sal";
        public static string FindFreeRooms => "Znajdź wolną salę";
        public static string Timeschedule => "Rozkład zajęć";
        public static string EditConsultations => "Edytuj terminy konsultacji";
        public static string Consultations => "Zestawienie konsultacji";

    }
}