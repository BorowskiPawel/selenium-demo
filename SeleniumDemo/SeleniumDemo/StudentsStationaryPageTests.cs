using System.Linq;
using OpenQA.Selenium;
using Xunit;

namespace SeleniumDemo
{
    public class StudentsStationaryPageTests : BaseTest
    {
        [Fact]
        public void Title_Test()
        {
            // Arrange
            var expectedTitle = "Rozkład zajęć studentów i nauczycieli Wydziału Informatyki";
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Students.Stationary);

            //Assert
            Assert.True(_driver.Title == expectedTitle);
            _driver.Close();
        }

        [Fact]
        public void PDF_Links_Test()
        {
            // Arrange
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Students.Stationary);
            var listItems = _driver.FindElements(By.TagName("li"));

            //Assert
            foreach (var item in listItems)
            {
                var links = item.FindElements(By.TagName("a")).Where(x => x.GetProperty("href").Contains(".pdf"));
                foreach (var link in links)
                {
                    var href = link.GetProperty("href");
                    Assert.NotNull(href);
                    link.Click();
                    // test pdf???
                }
            }

            _driver.Close();
        }
    }
}