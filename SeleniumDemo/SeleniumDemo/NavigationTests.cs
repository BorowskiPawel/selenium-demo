using System.Linq;
using OpenQA.Selenium;
using SeleniumDemo.Constants;
using Xunit;

namespace SeleniumDemo
{
    public class NavigationTests : BaseTest
    {
        private IWebElement GetDropdownOption(IWebDriver driver, string dropdownType ,string optionText)
        {

            var dropdownToExpand = _driver.FindElements(By.ClassName("dropdown"))
                .First(x => x.Text == dropdownType);
            dropdownToExpand.Click();

            var dropdownOption = dropdownToExpand.FindElement(By.ClassName("dropdown-menu"))
                .FindElements(By.ClassName("dropdown-item"))
                .First(x => x.Text == optionText);

            return dropdownOption;
        }

        [Fact]
        public void Navigate_Home_Page()
        {
            // Arrange
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Base);
            var homeNavItem = _driver.FindElements(By.ClassName("nav-link"))
                                     .First(x => x.Text == NavigationDropdownLabels.Home);
            homeNavItem.Click();
            
            //Assert
            Assert.Equal(this._driver.Url, Constants.Urls.Home);
        }
        
        [Fact]
        public void Navigate_Stationary_Page()
        {
            // Arrange
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Base);
            GetDropdownOption(_driver, NavigationDropdownTypes.Students, NavigationDropdownLabels.StationaryStudies).Click();

            //Assert
            Assert.Equal(this._driver.Url, Constants.Urls.Students.Stationary);
        }
        
        [Fact]
        public void Navigate_NonStationary_Page()
        {
            // Arrange
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Base);
            GetDropdownOption(_driver, NavigationDropdownTypes.Students, NavigationDropdownLabels.NonStationaryStudies).Click();
            
            //Assert
            Assert.Equal(this._driver.Url, Constants.Urls.Students.NonStationary);
        }
        
        [Fact]
        public void Navigate_AfterDiploma_Page()
        {
            // Arrange
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Base);
            GetDropdownOption(_driver, NavigationDropdownTypes.Students, NavigationDropdownLabels.AfterDiplomaStudies).Click();
            
            //Assert
            Assert.Equal(this._driver.Url, Constants.Urls.Students.AfterDiploma);
        }
        
        [Fact]
        public void Navigate_TeacherSchedule_Page()
        {
            // Arrange
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Base);
            GetDropdownOption(_driver, NavigationDropdownTypes.Students, NavigationDropdownLabels.TeacherSchedule).Click();
            
            //Assert
            Assert.Equal(this._driver.Url, Constants.Urls.Students.TeacherSchedule);
        }
        
        [Fact]
        public void Navigate_RoomsAvailiability_Page()
        {
            // Arrange
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Base);
            GetDropdownOption(_driver, NavigationDropdownTypes.Teachers, NavigationDropdownLabels.RoomsAvailiability).Click();
            
            //Assert
            Assert.Equal(this._driver.Url, Constants.Urls.Teachers.RoomsAvailiability);
        }
        
        [Fact]
        public void Navigate_FindFreeRooms_Page()
        {
            // Arrange
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Base);
            GetDropdownOption(_driver, NavigationDropdownTypes.Teachers, NavigationDropdownLabels.FindFreeRooms).Click();
            
            //Assert
            Assert.Equal(this._driver.Url, Constants.Urls.Teachers.FindFreeRooms);
        }
        
        [Fact]
        public void Navigate_Timeschedule_Page()
        {
            // Arrange
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Base);
            GetDropdownOption(_driver, NavigationDropdownTypes.Teachers, NavigationDropdownLabels.Timeschedule).Click();
            
            //Assert
            Assert.Equal(this._driver.Url, Constants.Urls.Teachers.Timeschedule);
        }
        
        [Fact]
        public void Navigate_EditConsultations_Page()
        {
            // Arrange
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Base);
            GetDropdownOption(_driver, NavigationDropdownTypes.Teachers, NavigationDropdownLabels.EditConsultations).Click();
            
            //Assert
            Assert.Equal(this._driver.Url, Constants.Urls.Teachers.EditConsultations);
        }
        
        [Fact]
        public void Navigate_Consultations_Page()
        {
            // Arrange
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Base);
            GetDropdownOption(_driver, NavigationDropdownTypes.Teachers, NavigationDropdownLabels.Consultations).Click();
            
            //Assert
            Assert.Equal(this._driver.Url, Constants.Urls.Teachers.Consultations);
        }
    }
}