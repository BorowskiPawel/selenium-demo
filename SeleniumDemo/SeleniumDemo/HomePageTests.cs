using Xunit;

namespace SeleniumDemo
{
    public class HomePageTests : BaseTest
    {
        [Fact]
        public void Title_Test()
        {
            // Arrange
            var expectedTitle = "Rozkład zajęć studentów i nauczycieli Wydziału Informatyki";
            _driver.Manage().Window.Maximize();

            // Act
            _driver.Navigate().GoToUrl(Constants.Urls.Home);

            //Assert
            Assert.True(_driver.Title == expectedTitle);
            _driver.Close();
        }
    }
}